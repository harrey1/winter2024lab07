import java.util.Random;
public class Deck{
	
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	
	public Deck(){
		
		this.rng = new Random();
		this.cards = new Card[52];
		this.numberOfCards = 52;
		
		String[] suits = {"Hearts", "Diamonds", "Spades", "Clubs"};
		String[] values = {"Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"};
		
		for(int i = 0; i < values.length; i++){
			for(int j = 0; j < suits.length; j++){
				this.cards[i + 13*j] = new Card(suits[j], values[i]);
			}
		}
	}
	
	public int length(){
		

		return this.numberOfCards;
		
	}
	
	public Card drawTopCard(){
		
		this.numberOfCards--;
		return this.cards[this.numberOfCards];
		
	}
	
	public String toString(){
		
		String mycard = "";
		for(int i = 0; i < this.numberOfCards; i++){
			mycard += this.cards[i] + "\n" ;
		}
		return mycard;
		
	}
	
	public void shuffle(){
		for(int i = 0; i < this.numberOfCards; i++){
			int pos = i + this.rng.nextInt(this.numberOfCards - i);
			Card save = this.cards[i];
			this.cards[i] = this.cards[pos];
			this.cards[pos] = save;
		}
	}
	
	
}