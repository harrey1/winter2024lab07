public class SimpleWar{
	public static void main(String[] args){
		
		Deck myDeck = new Deck();
		myDeck.shuffle();
		
		
		int P1points = 0;
		int P2points = 0;
		int i = 0;
		
		while(i < myDeck.length()){
		
			Card card1 = myDeck.drawTopCard();
			Card card2 = myDeck.drawTopCard();
			
			System.out.println("--------------------------" + "\n" + "Current Points");
			System.out.println("Points: " + P1points);
			System.out.println("Points: " + P2points);
			System.out.println("--------------------------");
			
			System.out.println(card1 + ": " + card1.calculateScore());
			System.out.println(card2 + ": " + card2.calculateScore());
			
			if(card1.calculateScore() > card2.calculateScore()){
				System.out.println("Player 1 wins!");
				P1points += 1;
			} else {
				System.out.println("Player 2 wins!");
				P2points += 1;
			}
			
			System.out.println("Player 1 Points: " + P1points);
			System.out.println("Player 2 Points: " + P2points);
		}
		
		System.out.println("--------------------------");
		
		if(P1points > P2points){
			System.out.println("Player 1 wins!");
		} else {
			System.out.println("Player 2 wins!");
		}
		
	}
}