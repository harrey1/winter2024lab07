public class Card{
	
	private String suit;
	private String value;
	
	public Card(String suit, String value){
		
		this.suit = suit;
		this.value = value;
		
	}
	
	public String getSuit(){
		return this.suit;
	}
	
	public String getValue(){
		return this.value;
	}
	
	public String toString(){
		return this.value + " of " + this.suit;
	}
	
	public double calculateScore(){
		double score = 0;
		
		if(this.value.equals("Ace")){
			score += 1;
		} else if(this.value.equals("Two")){
			score += 2;
		} else if(this.value.equals("Three")){
			score += 3;
		} else if(this.value.equals("Four")){
			score += 4;
		} else if(this.value.equals("Five")){
			score += 5;
		} else if(this.value.equals("Six")){
			score += 6;
		} else if(this.value.equals("Seven")){
			score += 7;
		} else if(this.value.equals("Eight")){
			score += 8;
		} else if(this.value.equals("Nine")){
			score += 9;
		} else if(this.value.equals("Ten")){
			score += 10;
		} else if(this.value.equals("Jack")){
			score += 11;
		} else if(this.value.equals("Queen")){
			score += 12;
		} else {
			score += 13;
		}
		
		if(this.suit.equals("Hearts")){
			score += 0.4;
		} else if(this.suit.equals("Spades")){
			score += 0.3;
		} else if(this.suit.equals("Diamonds")){
			score += 0.2;
		} else {
			score += 0.1;
		}
		
		return score;
	}
}